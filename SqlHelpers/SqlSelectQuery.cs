﻿using SqlHelpers.Fluent;
using SQLHelpers.Fluent;
using System.Collections.Generic;

namespace SqlHelpers
{
  public class SqlSelectQuery
  {
    private SqlSelectQuery()
    {
      SelectedItems = new List<string>();
      WhereConditions = new List<string>();
      OrderByConditions = new List<string>();
      GroupByConditions = new List<string>();
    }

    public List<string> SelectedItems { get; }
    public string SelectedTable { get; set; }
    public List<string> WhereConditions { get; set; }
    public List<string> OrderByConditions { get; set; }
    public List<string> GroupByConditions { get; set; }

    public static ISelect Select(params string[] items)
    {
      return new SqlSelectQueryFluent(new SqlSelectQuery(), items);
    }
    public static ISelect SelectAll()
    {
      return new SqlSelectQueryFluent(new SqlSelectQuery());
    }
    public string ToQuery()
    {
      var queryFluent = new SqlSelectQueryFluent(this);
      return queryFluent.ToQuery();
    }
  }
}
