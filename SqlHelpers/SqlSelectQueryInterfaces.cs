﻿using SqlHelpers;

namespace SQLHelpers.Fluent
{
  public interface ISelect
  {
    IFrom From(string tableName);
  }
    
  public interface IFrom
  {
    IWhere Where(string condition);
    IOrderBy OrderBy(string orderByCondition);
    IGroupBy GroupBy(string groupByCondition);
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }

  public interface IWhere
  {
    IOrderBy OrderBy(string orderByCondition);
    IGroupBy GroupBy(string groupByCondition);
    IOr Or();
    IAnd And();
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }
  
  public interface IGroupBy
  {
    IOrderBy OrderBy(string orderByCondition);
    IGroupByAsc Asc();
    IGroupByDesc Desc();
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }

  public interface IOrderBy
  {
    IGroupBy GroupBy(string groupByCondition);
    IOrderByAsc Asc();
    IOrderByDesc Desc();
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }

  public interface IAnd
  {
    IWhere Where(string condition);
    IOrderBy OrderBy(string orderByCondition);
    IGroupBy GroupBy(string groupByCondition);
  }

  public interface IOr
  {
    IWhere Where(string condition);
    IOrderBy OrderBy(string orderByCondition);
    IGroupBy GroupBy(string groupByCondition);
  }
  public interface IOrderByAsc : IAsc { }
  public interface IOrderByDesc : IDesc { }
  public interface IGroupByAsc : IAsc { }
  public interface IGroupByDesc : IDesc { }
  public interface IAsc
  {
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }

  public interface IDesc
  {
    SqlSelectQuery ToQueryObject();
    string ToQuery();
  }
}
