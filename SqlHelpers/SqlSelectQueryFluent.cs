﻿using SQLHelpers.Fluent;
using System.Collections.Generic;
using System.Linq;

namespace SqlHelpers.Fluent
{
  internal class SqlSelectQueryFluent
    : ISelect, IWhere, IFrom, IGroupBy, IOrderBy, IOr, IAnd, IGroupByAsc, IGroupByDesc, IOrderByAsc, IOrderByDesc
  {
    private readonly SqlSelectQuery _constructedQuery;

    public SqlSelectQueryFluent(SqlSelectQuery constructedQuery)
    {
      _constructedQuery = constructedQuery;
    }

    public SqlSelectQueryFluent(SqlSelectQuery constructedQuery, string[] selectedItems)
        : this(constructedQuery)
    {
      _constructedQuery.SelectedItems.AddRange(selectedItems);
    }

    public IFrom From(string tableName)
    {
      _constructedQuery.SelectedTable = tableName;
      return this;
    }
    public IWhere Where(string condition)
    {
      _constructedQuery.WhereConditions.Add(condition);
      return this;
    }
    public IOrderBy OrderBy(string orderByCondition)
    {
      _constructedQuery.OrderByConditions.Add(orderByCondition);
      return this;
    }
    public IGroupBy GroupBy(string groupByCondition)
    {
      _constructedQuery.GroupByConditions.Add(groupByCondition);
      return this;
    }
    public IOr Or()
    {
      var item = _constructedQuery.WhereConditions.Last();
      _constructedQuery.WhereConditions.Remove(item);
      item = string.Concat(item, " OR");
      _constructedQuery.WhereConditions.Add(item);
      return this;
    }
    public IAnd And()
    {
      var item = _constructedQuery.WhereConditions.Last();
      _constructedQuery.WhereConditions.Remove(item);
      item = string.Concat(item, " AND");
      _constructedQuery.WhereConditions.Add(item);
      return this;
    }
    IGroupByAsc IGroupBy.Asc()
    {
      LastByAsc(_constructedQuery.GroupByConditions);
      return this;
    }
    IGroupByDesc IGroupBy.Desc()
    {
      LastByDesc(_constructedQuery.GroupByConditions);
      return this;
    }
    IOrderByAsc IOrderBy.Asc()
    {
      LastByAsc(_constructedQuery.OrderByConditions);
      return this;
    }
    IOrderByDesc IOrderBy.Desc()
    {
      LastByDesc(_constructedQuery.OrderByConditions);
      return this;
    }
    public string ToQuery()
    {
      return string.Concat(GetSelect(), GetFrom(), GetWhere(), GetOrderBy(), GetGroupBy());
    }

    private string GetGroupBy()
    {
      if (_constructedQuery.GroupByConditions.Any())
        return string.Format("GROUP BY {0} ", string.Join(" ", _constructedQuery.GroupByConditions));
      else
        return string.Empty;
    }

    private string GetOrderBy()
    {
      if (_constructedQuery.OrderByConditions.Any())
        return string.Format("ORDER BY {0} ", string.Join(" ", _constructedQuery.OrderByConditions));
      else
        return string.Empty;
    }

    private string GetWhere()
    {
      if (_constructedQuery.WhereConditions.Any())
        return string.Format("WHERE {0} ", string.Join(" ", _constructedQuery.WhereConditions));
      else
        return string.Empty;
    }

    private string GetFrom()
    {
      return string.Format("FROM {0} ", _constructedQuery.SelectedTable);
    }

    private string GetSelect()
    {
      var isSelectAll = _constructedQuery.SelectedItems.Count == 0;

      if (isSelectAll)
        return "SELECT * ";
      else
        return string.Format("SELECT {0} ", string.Join(", ", _constructedQuery.SelectedItems));
    }
    
    private void LastByAsc(List<string> collection)
    {
      var item = collection.Last();
      collection.Remove(item);
      item = string.Concat(item, " ASC");
      collection.Add(item);
    }

    private void LastByDesc(List<string> collection)
    {
      var item = collection.Last();
      collection.Remove(item);
      item = string.Concat(item, " DESC");
      collection.Add(item);
    }

    public SqlSelectQuery ToQueryObject()
    {
      return _constructedQuery;
    }
  }
}
