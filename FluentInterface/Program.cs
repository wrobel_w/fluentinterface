﻿using SqlHelpers;
using System;

namespace FluentInterface
{
  class Program
  {
    static void Main(string[] args)
    {
      var selectQuery = SqlSelectQuery.Select("Id", "FirstName")
        .From("Employee")
        .Where("LastName = 'Wróbel'")
        .And()
        .Where("Age = 6")
        .ToQuery();

      SqlSelectQuery selectAllQuery = SqlSelectQuery.SelectAll()
        .From("Carts")
        .GroupBy("Category")
        .ToQueryObject();

      var selectAllQueryAsString = selectAllQuery.ToQuery();

      Console.WriteLine(selectQuery);
      Console.WriteLine(selectAllQueryAsString);
      Console.ReadKey();
    }
  }
}
